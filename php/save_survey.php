<?php
	if($_GET['theme']){
		$theme = $_GET['theme'];
	} else {
		echo "Не передана тема";
		http_response_code(400);
		exit();
	}
	if($_GET['variants']) {
		$variants = explode("|", $_GET['variants']);
	} else {
		echo "Не переданы варианты";
		http_response_code(400);
		exit();
	}

    include ("php/connect_database.php");

	$link->begin_transaction();
	$a1 = $link->query("INSERT INTO surveys (name) VALUES (\"".$theme."\")");
	$id = $link->insert_id;
	$a2 = true;
	foreach ($variants as $variant) {
		$a2 = $a2 and $link->query("INSERT INTO survey_variants (variant, survey_id) VALUES (\"".$variant."\", \"".$id."\")");
	}
	if ($a1 and $a2) {
		$link->commit();
    	http_response_code(200);
		echo "Тема ".$theme." сохранена.\n";
		echo "Варианты:\n";
		foreach ($variants as $value) {
			echo $value."\n";
		}
	} else {
		echo "SQL Error: ".$link->error;
		$link->rollback();
    	http_response_code(500);
	}

	$link->close();
?>