<?php
    if($_GET['search']){
        $search = $_GET['search'];
    } else {
	    echo "Не передан параметр поиска";
        http_response_code(400);
        exit();
    } 

    include ("php/connect_database.php");
    
    try {
	    $q1 = $link->query("SELECT * FROM surveys WHERE name like \"%".$search."%\"");
        if ($q1) {
            $result = $q1->fetch_all();
        } else {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        
        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($result);
    } finally {
        $link->close();
    }
?>