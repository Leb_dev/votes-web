<?php
    if (isset($_COOKIE['login']) and isset($_COOKIE['password'])) {
        $login = $_COOKIE['login'];
        $password = $_COOKIE['password'];
        include("connect_database.php");
        try {
            $result = $link->query("SELECT password FROM USERS WHERE login=\"$login\" and activated =1")->fetch_all();
            $err = $link->error;
            if ($err != "") {
                header("Location: ../index.html");
                die();
            }
            if (!password_verify($password, $result[0][0])) {
                header("Location: ../index.html");
                die();
            }
        } finally {
            $link->close;
        }
    } else {
        header("Location: ../index.html");
        die();
    }
?>