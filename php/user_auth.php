<?php
    $action = $_POST['action'];
    #$action = "Зарегистрироваться";
    #$action = "Войти";
    $login = $_POST['login'];
    $login = stripslashes($login);
    $login = htmlspecialchars($login);
    $login = trim($login);
    $password = $_POST['password'];
    $password = stripslashes($password);
    $password = htmlspecialchars($password);
    $password = trim($password);
    if ($login == "" or $password == "") {
        echo "Логин и/или пароль пусты";
        exit();
    }
    
    if ($action == "Зарегистрироваться") {
        include ("connect_database.php");
        try {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $link->query("INSERT INTO users (login, password) VALUES (\"".$login."\", \"".$password."\")");
            $err = $link->error;
            if ($err != "") {
                echo $err;
                exit();
            }
            
            $headers  = "Content-type: text/html; charset=UTF-8\r\n"; 
            $headers .= "From: Velemis@yandex.ru";
            $reglink = $HTTP_HOST."/php/activate_user.php?login=$login&hash=$password";
            mail($login, "Регистрация на ".$HTTP_HOST, "Ваш логин: Пароль: Подтвердите регистрацию на сайте, перейдя по ссылке: ".$reglink, $headers);
            echo "На почту $login отправлено письмо для подтверждения регистрации <br>";
            echo "<a href=$reglink>$reglink</a>";
        } finally {
            $link->close;
        }
    } elseif ($action == "Войти") {
        include ("connect_database.php");
        try {
            $result = $link->query("SELECT password FROM USERS WHERE login=\"$login\" and activated =1")->fetch_all();
            $err = $link->error;
            if ($err != "") {
                echo $err;
                exit();
            }
            if (password_verify($password, $result[0][0])) {
                setcookie("login", $login, time()+3600, "/");
                setcookie("password", $password, time()+3600, "/");
                header("Location: ../main.html");
            } else {
                echo "Неверный логин/пароль";
            }
        } finally {
            $link->close;
        }
    } else {
        http_response_code(400);
        echo "Неизвестное действие";
    }
?>