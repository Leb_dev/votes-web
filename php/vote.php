<?php
    if($_GET['variant_id']){
        $variant_id = $_GET['variant_id'];
    } else {
        echo "Не передан id варианта";
        http_response_code(400);
        exit();
    }
    $login = $_COOKIE['login'];
    
    include ("check_logged.php");
    include ("connect_database.php");
    
    try {
        //Голосовал ли пользователь за этот вариант
	    $q = $link->query("SELECT voter_id FROM votes WHERE variant_id=$variant_id and voter_id=(SELECT _id from users WHERE login=\"$login\" LIMIT 1)");
        if ($q) {
            $voter_id = $q->fetch_all()[0][0];
        } else {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        if ($voter_id != "") {
            echo "Вы уже голосовали за этот вариант!";
            http_response_code(400);
            exit();
        }
        
        //ID пользователя
        $q = $link->query("SELECT _id FROM users WHERE login=\"$login\"");
        if ($q) {
            $voter_id = $q->fetch_all()[0][0];
        } else {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        
        //Выбираем все варианты, относящиеся к теме
        $q = $link->query("SELECT _id FROM survey_variants where survey_id=(SELECT survey_id FROM survey_variants WHERE _id=$variant_id LIMIT 1)");
        if ($q) {
            $ids = $q->fetch_all();
        } else {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        
        $list = [];
        foreach($ids as $row) {
            array_push($list, $row[0]);
        }
        
        //Удаляем все голоса пользователя по этой теме
        if (!$link->query("DELETE FROM votes where variant_id in (".implode(", ", $list).") and voter_id=$voter_id")) {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        
        //Голосуем
        if (!$link->query("INSERT INTO votes (variant_id, voter_id) VALUES ($variant_id, $voter_id)")) {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
        
        //Запрашиваем результаты
        $q = $link->query("SELECT * FROM votes_count WHERE variant_id in (".implode(", ", $list).") order by variant_id");
        if ($q) {
            $res = "";
            foreach ($q->fetch_all() as $row) {
                $res .= $row[0].",".$row[1].";";
            }
            if ($res != "") {
                $res = substr_replace($res, "", -1);
                echo $res;
            } else {
                echo "No results!";
                http_response_code(500);
            }
        } else {
            echo "SQL Error: ".$link->error;
        	http_response_code(500);
            exit();
        }
    } finally {
        $link->close();
    }
?>